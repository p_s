#!/usr/bin/env perl
use strict;
use warnings;

# code as grid. only the grid saves!
# there may already be a little groff markup in this file
my $textFile = "sections_7,_9" unless $ARGV[0];
$textFile = $ARGV[0] unless not $ARGV[0];

# this returns the text as an array of arrays, alllinesAllleters ;)
my @textAoA = &parseText($textFile);
&horizontalMotionMarkUp(\@textAoA, 20);

# takes as an argument the file to be parsed, so &parseText("file")
sub parseText {
    my @alllinesAllletters;
    my @currentLineLetters;
    open(TEXT, "<", $_[0]) || die "can't open $_[0] : $!";

    while (<TEXT>) {
        chomp;
        @currentLineLetters = split //, $_;
        push @alllinesAllletters, [@currentLineLetters];
    }
    @alllinesAllletters;
}

sub horizontalMotionMarkUp {
    my @textLetters = @{$_[0]};
	my $localMotionRange = $_[1];
    my $aLetter;
    my $aLineRef;
    my $anAmount;
    my $markedUpLetter;

    # this is the file that we will build
    my $newFile = $textFile . "Typeset";
    open(TYPESET, ">", $newFile) || die "can't open $newFile : $!";

    # for evry letter...
    foreach $aLineRef (@textLetters) {
        if (@$aLineRef == 0) {}
        # the script recognizes lines that begin with a `.' or a `\'
		elsif (@$aLineRef[0] eq "." || @$aLineRef[0] eq "\\") {
            foreach $aLetter (@{$aLineRef}) {
                print TYPESET $aLetter;
            }
        } else {
            foreach $aLetter (@{$aLineRef}) {
                $anAmount = int(rand($localMotionRange));
                $markedUpLetter = "\\h'" . $anAmount . "M'$aLetter\\h'-" . $anAmount . "M'";
                print TYPESET $markedUpLetter;
            }
        }
        print TYPESET "\n";
    }
}
